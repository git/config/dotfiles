# Resume vim with C-z
foreground-vi() {
    fg %vi
}
zle -N foreground-vi
bindkey '^Z' foreground-vi

# Search full zsh docs
zman() {
    PAGER="less -g -s '+/^       "$1"'" man zshall
}

# Prepend sudo

prepend-sudo() {
	if [[ "$BUFFER" != su(do|)\ * ]]; then
		BUFFER="sudo $BUFFER"
		(( CURSOR += 5 ))
	fi
}
zle -N prepend-sudo
bindkey '^S' prepend-sudo

sudo-command-line() {
    [[ -z $BUFFER ]] && zle up-history
    [[ $BUFFER != sudo\ * ]] && BUFFER="sudo ${BUFFER% }"
    zle end-of-line
}
zle -N sudo-command-line
bindkey "\e\e" sudo-command-line

# Directory helper - stolen from the Internet
# I don't have a bloody clue how this spaghetti works
# http://chneukirchen.org/blog/category/zsh.html
# `up N` to go up N dirs
# `up fud` to go to nearest dir with 'fud' in name

upp() {
	local op=print
	[[ -t 1 ]] && op=cd
	case "$1" in
		'') upp 1;;
		-*|+*) $op ~$1;;
		<->) $op $(printf '../%.0s' {1..$1});;
		*)	local -a seg; seg=(${(s:/:)PWD%/*})
			local n=${(j:/:)seg[1,(I)$1*]}
			if [[ -n $n ]]; then
				$op /$n
			else
				print -u2 upp: could not find prefix $1 in $PWD
				return 1
			fi
	esac
}
zle -N upp

# colour conversion
hex2rgb() {
    printf '%d %d %d\n' "0x${1:0:2}" "0x${1:2:2}" "0x${1:4:2}"
}
rgb2hex() {
    printf '%02X%02X%02X\n' "$1" "$2" "$3"
}

# paste to ix.io
# bloody good pastebin m9
ix() {
    local opts
    local OPTIND
    [ -f "$HOME/.netrc" ] && opts='-n'
    while getopts ":hd:i:n:" x; do
        case $x in
            h) echo "ix [-d ID] [-i ID] [-n N] [opts]"; return;;
            d) $echo curl $opts -X DELETE ix.io/$OPTARG; return;;
            i) opts="$opts -X PUT"; local id="$OPTARG";;
            n) opts="$opts -F read:1=$OPTARG";;
        esac
    done
    shift $(($OPTIND - 1))
    [ -t 0 ] && {
        local filename="$1"
        shift
        [ "$filename" ] && {
            curl $opts -F f:1=@"$filename" $* ix.io/$id
            return
        }
        echo "^C to cancel, ^D to send."
    }
    curl $opts -F f:1='<-' $* ix.io/$id
}

# adjust file timestamps
# handy if your camera had an inaccurate clock
# usage: 
#     tsmod "- 1 hour" ./*.JPG
#     tsmod "+ 2 hours" ./*
tsmod() {
    local modstr="$1"
    shift
    for fil in $@; do touch -d "$(date -R -r $fil) $modstr" $fil; done
}


# video streaming script glue
# TODO: Integrate this into THE SCRIPTS THEMSELVES
function youc() { local res=$(youchannel $*); [ $? = 0 ] && mpv $(echo $res) }
function yous() { local res=$(yousearch $*);  [ $? = 0 ] && mpv $(echo $res) }
function twitch() { mediaplay twitchfollow $* }
function mediaplay() {
	local mediaplayer="mpv"
	local mediaurlcmd="$1"
	local vopar=""
	local ytdlpar=""
	shift
	if [ -n "$1" ] && [[ ! "$1" =~ '-' ]]; then
		ytdlpar="--ytdl-format=$1"
		shift
	fi
	if [[ $(tty) =~ "tty" ]] || [[ $(tmux list-panes -F '#{session_name}') =~ "tty" ]]; then
		vopar="--vo=drm,tct,caca"
	fi
	local res=$($mediaurlcmd $*)
	[ $? = 0 ] && $mediaplayer $ytdlpar $vopar $(echo $res)
}
