"""
i3pystatus stopwatch module
"""
import time

from i3pystatus import IntervalModule
from i3pystatus.core.util import TimeWrapper


class StopwatchState:
    """
    Stopwatch state values.
    """
    stopped = 0
    running = 1
    paused = 2


class Stopwatch(IntervalModule):
    """
    Stopwatch module for time .

    Main features include:

    - Start, stop, pause and adjust the stopwatch with click events.
    - Different output formats can be triggered when time is more than `x` seconds.

    .. rubric:: Available formatters

    Time formatters are available to show the running time.
    These include ``%h``, ``%m``, ``%s``, ``%H``, ``%M``, ``%S``.
    See :py:class:`.TimeWrapper` for detailed description.

    The ``format_custom`` setting allows you to display different formats when
    certain amount of seconds are reached.
    This setting accepts a list of tuples which contain the minimum time in seconds,
    format string, color string and (optional) paused format string each.
    See the default setting for an example:

    - ``(3600, "%h:%M:%S", "#00ff00", "%h:%M:%S")`` - Show hour when time is over one hour.

    Only first matching rule is applied (if any).

    .. rubric:: Callbacks

    Module contains three mouse event callback methods:

    - :py:meth:`.toggle` - Default: Left click starts stopwatch, or pauses.
    - :py:meth:`.increase` - Default: Upscroll/downscroll increase/decrease time
      by 1 minute.
    - :py:meth:`.reset` - Default: Right click resets stopwatch.

    """

    interval = 1

    on_leftclick = ["toggle"]
    on_rightclick = ["reset"]
    on_upscroll = ["increase", 60]
    on_downscroll = ["increase", -60]

    settings = (
        ("format", "Default format, shown if no ``format_custom`` "
         "rules are matched."),
        ("format_paused", "Format showed when stopwatch is paused."),
        ("format_stopped", "Format showed when stopwatch is inactive."),
        "format_custom",
        "color",
        "color_paused",
        "color_stopped",
    )

    format = '%M:%S'
    format_paused = '%M:%S'
    format_stopped = "00:00"
    format_custom = [(3600, "%h:%M:%S", "#ffffff", "%h:%M:%S")]
    color = "#ffffff"
    color_paused = "#00ff00"
    color_stopped = "#ffffff"

    def init(self):
        self.last_check = 0
        self.seconds = 0
        self.state = StopwatchState.stopped
        if not self.format_custom:
            self.format_custom = []

    def run(self):
        if self.state is not StopwatchState.stopped:
            fmt = self.format
            color = self.color

            if self.state is StopwatchState.running:
                self.seconds += time.time() - self.last_check
                self.last_check = time.time()
            if self.state is StopwatchState.paused:
                color = self.color_paused
                fmt = self.format_paused

            for rule in self.format_custom:
                if self.seconds > rule[0]:
                    if self.state is StopwatchState.paused:
                        if len(rule) > 3:
                            fmt = rule[3]
                    else:
                        fmt = rule[1]
                    color = rule[2]
                    break

            self.output = {
                "full_text": format(TimeWrapper(self.seconds, fmt)),
                "color": color,
                "urgent": self.state is StopwatchState.running,
            }
        else:
            self.output = {
                "full_text": self.format_stopped,
                "color": self.color_stopped,
            }

    def start(self):
        """
        Starts stopwatch.
        """
        if self.state is not StopwatchState.running:
            if self.state is StopwatchState.stopped:
                self.seconds = 1
            self.state = StopwatchState.running
            self.last_check = time.time()

    def pause(self):
        """
        Pauses stopwatch.
        """
        if self.state is StopwatchState.running:
            self.state = StopwatchState.paused
            self.seconds += time.time() - self.last_check
            self.last_check = 0

    def toggle(self):
        """
        Toggles stopwatch between started and paused.
        """
        if self.state is StopwatchState.running:
            self.pause()
        else:
            self.start()


    def increase(self, seconds):
        """
        Increase stopwatch running time.

        :param int seconds: Seconds to add. Negative values subtract from running time.
        """
        if self.state is StopwatchState.running and self.seconds + seconds > 0:
            self.seconds = self.seconds + seconds

    def reset(self):
        """
        Resets stopwatch.
        """
        if self.state is not StopwatchState.stopped:
            self.state = StopwatchState.stopped
            self.seconds = 0
            self.last_check = 0
