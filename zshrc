#!/usr/bin/env zsh -l

# 256-color
if ( [ "$TERM" = "xterm-termite" ] || [ "$TERM" = "screen-256color" ] || [[ "$TERM" =~ "rxvt" ]] || [ "$TERM" = "alacritty" ] || [ "$TERM" = "xterm-256color" ]) && [ -z "$TMUX" ]; then
    export TERM=xterm-256color
    base_session='term'
    client_cnt=$(tmux list-clients | grep $base_session | wc -l)
    if [ $client_cnt -ge 1 ] && ( [ ! $client_cnt -eq 1 ] || ! (tmux list-clients | grep -q "term-1") ); then
        session_name=$base_session"-"$((client_cnt))
        exec tmux new-session -A -s $session_name
    else
        exec tmux new-session -A -s ${base_session}
    fi
fi
if [ -e /usr/share/terminfo/x/xterm-256color ] && [ "$TERM" = "xterm" ]; then
    export TERM=xterm-256color
fi

# dircolors theme
eval `dircolors $HOME/.dircolors`

# Autocomplete options
autoload -Uz compinit
compinit -d $HOME/.tmp/zcompdump

zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*:complete:mpv:*' tag-order '!urls'
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' list-colors ''
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion::*:kill:*:*' command 'ps xf -U $USER -o pid,%cpu,cmd'
zstyle ':completion::*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;32'
setopt nocasematch 

# Prompt options
. $HOME/.zsh/prompt.zsh

# History options
HISTFILE=$HOME/.zhistory
HISTSIZE=15000
SAVEHIST=10000

setopt appendhistory histignorespace histignorealldups histfindnodups

# Directory options
setopt autocd autopushd pushdtohome pushdignoredups

# Command options
#setopt nobanghist

# create a zkbd compatible hash;
# to add other keys to this hash, see: man 5 terminfo
typeset -A key
key[Home]=${terminfo[khome]}
key[End]=${terminfo[kend]}
key[Insert]=${terminfo[kich1]}
key[Delete]=${terminfo[kdch1]}
key[Up]=${terminfo[kcuu1]}
key[Down]=${terminfo[kcud1]}
key[Left]=${terminfo[kcub1]}
key[Right]=${terminfo[kcuf1]}
key[PageUp]=${terminfo[kpp]}
key[PageDown]=${terminfo[knp]}
# setup key accordingly
[[ -n "${key[Home]}"    ]]  && bindkey  "${key[Home]}"    beginning-of-line
[[ -n "${key[End]}"     ]]  && bindkey  "${key[End]}"     end-of-line
[[ -n "${key[Insert]}"  ]]  && bindkey  "${key[Insert]}"  overwrite-mode
[[ -n "${key[Delete]}"  ]]  && bindkey  "${key[Delete]}"  delete-char
[[ -n "${key[Up]}"      ]]  && bindkey  "${key[Up]}"      up-line-or-history
[[ -n "${key[Down]}"    ]]  && bindkey  "${key[Down]}"    down-line-or-history
[[ -n "${key[Left]}"    ]]  && bindkey  "${key[Left]}"    backward-char
[[ -n "${key[Right]}"   ]]  && bindkey  "${key[Right]}"   forward-char
# history search with PageUp/PageDown keys
[[ -n "${key[PageUp]}"   ]]  && bindkey  "${key[PageUp]}"    history-beginning-search-backward
[[ -n "${key[PageDown]}" ]]  && bindkey  "${key[PageDown]}"  history-beginning-search-forward
# backspace and ^h working even after
# returning from command mode
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char
# ctrl-w removed word backwards
bindkey '^w' backward-kill-word
# ctrl-r starts searching history backward
bindkey '^r' history-incremental-search-backward
# st lies in $TERM to work with vim, this covers for it
bindkey '[1~' beginning-of-line
bindkey '[4~' end-of-line

# in menu selection ctrl+space to go to subdirectories
bindkey '^@' accept-and-infer-next-history

# insert mode in RPROMPT
function mode_rprompt {
	VIM_PROMPT="%B%F{yellow} [% NORMAL]% %f%b"
	RPROMPT="${${KEYMAP/vicmd/$VIM_PROMPT}/(main|viins)/}"
	zle reset-prompt
}
# make sure the terminal is in application mode when zle is active
# mode_rprompt needs to hook into zle-line-init, so it's here for now
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
    function zle-line-init () {
        echoti smkx
        mode_rprompt
    }
    function zle-keymap-select {
        mode_rprompt
    }
    function zle-line-finish () {
        echoti rmkx
    }
    zle -N zle-line-init
    zle -N zle-keymap-select
    zle -N zle-line-finish
fi

# Functions
. $HOME/.zsh/functions.zsh

# Aliases
. $HOME/.zsh/alias.zsh

# Environment
. $HOME/.zsh/env.zsh
