# https://github.com/sindresorhus/pure

# Change this to your own username
DEFAULT_USERNAME='simon'

# Threshold (sec) for showing cmd exec time
CMD_MAX_EXEC_TIME=5

# For my own and others sanity
# git:
# %b => current branch
# %a => current action (rebase/merge)
# prompt:
# %F => color dict
# %f => reset color
# %~ => current path
# %* => time
# %n => username
# %m => shortname host
# %(?..) => prompt conditional - %(condition.true.false)

# Only show username if not default
if [ $USER != $DEFAULT_USERNAME ]; then
	local username='%n@%m '
else
	local username='@%m '
	autoload -Uz vcs_info
	zstyle ':vcs_info:*' enable git hg
	zstyle ':vcs_info:*' formats ' %s:%b'
	zstyle ':vcs_info:*' actionformats ' %s:%b|%a'
fi

# Fastest possible way to check if repo is dirty
git_dirty() {
	git diff --quiet --ignore-submodules HEAD 2>/dev/null; [ $? -eq 1 ] && echo '*'
}

# Displays the exec time of the last command if set threshold was exceeded
cmd_exec_time() {
	local stop=`date +%s`
	local start=${cmd_timestamp:-$stop}
	let local elapsed=$stop-$start
	[ $elapsed -gt $CMD_MAX_EXEC_TIME ] && echo ${elapsed}s
}

preexec() {
	cmd_timestamp=`date +%s`
}

precmd() {
	[ $USER = $DEFAULT_USERNAME ] && vcs_info && local vcsmsg="$vcs_info_msg_0_`git_dirty`"
	print -P "\n%B%F{cyan}%~%f%F{green}$vcsmsg $username%f %F{yellow}`cmd_exec_time`%f%b"
	unset cmd_timestamp
}
disgit() {
	unset precmd
}

# Prompt turns red if the previous command didn't exit with 0
PROMPT='%B%(?.%F{green}.%F{red})%(!.#.>)%f%b '
[[ "$TERM" == *"256color" ]] && PROMPT=$(echo "$PROMPT" | sed 's/>/❯/')
