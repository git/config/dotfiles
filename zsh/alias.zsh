# Aliases

# Gentoo package management aliases
if [ -x /usr/bin/emerge ]; then
	if hash eix 2>/dev/null; then
		emscmd='sudo eix-sync'
		emfcmd='eix'
		emFcmd='eix -Ss'
	else
		emscmd='em --sync'
		emfcmd='emerge -s'
		emFcmd='emerge -S'
	fi
	hash layman 2>/dev/null && laycmd='sudo layman -S' || laycmd='echo'
	hash revdep-rebuild 2>/dev/null && revcmd='sudo revdep-rebuild' || revcmd='echo'

	alias em='sudo emerge'
	alias ems="$laycmd; $emscmd"
	alias emc='em -ac' #depclean
	alias emr="$revcmd"
	alias emu="em -auDN world && emc && emr"
	alias emf="$emfcmd "
	alias emF="$emFcmd "
	alias emp='emerge -pv '
	alias emup='emerge -pvuDN world'
	alias packmask="sudo $EDITOR /etc/portage/package.unmask"
	alias packuse="cd /etc/portage/package.use/"
	alias packkey="cd /etc/portage/package.accept_keywords/"

	unset eixcmd emfcmd emFcmd laycmd revcmd

	function emthing() {
		whilekillproc=$(pgrep emerge)
		while ps cax | grep -q $whilekillproc; do
			sudo echo wait... && sleep 30
		done
		date > ~/finished
		sleep 30 && sudo $@
		unset whilekillproc
	}
    function emhalt() {
        emthing shutdown -h now
    }
    function emsleep() {
        emthing s2mem
    }
fi

hash open 2>/dev/null && alias open=' open' || alias open=' xdg-open'

# keep safe after hacking gibsons
# there is no other required countermeasure
# alias priv='unset HISTFILE'
# ACTUALLY i'm getting paranoid
alias priv=' true'
# the typical ls shortcuts
alias ls='ls -vN --group-directories-first --color=auto'
alias l='ls'
alias la='l -A'
alias ll='l -hl'
alias lal='la -hl'
alias lr='l -R'
# for when you want to know what to delete
alias fs=' du -ah --max-depth=1 | sort -h'


# stop the 99% from making history
alias cd=' cd'
alias man=' man'
alias exit=' exit'
alias exec='priv && exec'

# ls on pwd change
function chpwd() {
	emulate -L zsh
	cnt=$(ls | wc -l)
	[[ $cnt -lt 150 ]] && ls || echo "Directory $(pwd) contains $cnt visible items"
}

# rtc wakeup
function rtcwake() {
	sudo sh -c "echo $(date '+%s' -d \"$*\") > /sys/class/rtc/rtc0/wakealarm"
}

# scp into a dropbear piece of crap
# $1 device to ssh, $2 dir to, $3+ items to copy
function dbscpto() {
	dvc=$1; remot="$2"; shift 2;
	tar cpf - $* | ssh $dvc "tar xpf - -C $remot"
	unset dvc remot
}
function dbscpfr() {
	dvc=$1; loca="$2"; shift 2;
	ssh $dvc "tar czpf - $*" | tar xzpf - -C $loca
	unset dvc loca
}

# qemu cd-only shortcut
function qemubase () {
	qemu-system-x86_64 \
	    -enable-kvm \
	    -m 2048 \
	    -nic user,model=virtio \
	    -cdrom $2 \
	    -display $1
}
function qemu () {
	qemubase gtk $*
}
function qemucli () {
	qemubase curses $*
}

# System-specific

# Thesaurus
alias thes=' aiksaurus'
# Torrents
alias tor='exec transmission-remote-cli'
# Irssi
alias irc=' priv; tmux new -s "irc" "irssi" || tmux a -t irc; exit'
# ff user
alias sf='sudo -u ff'
# host webserver
alias web='killall livereload || true && livereload --host 0.0.0.0 >/dev/null 2>&1 &disown'
# weather
alias weather='curl -k https://wttr.in/~Melbourne'

# esp-idf
alias esp-idf='. $HOME/.esp/esp-idf/export.sh'
