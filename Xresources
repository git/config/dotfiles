! global ---------------------------------------------------------------------

Xcursor.theme: Vanilla-DMZ-AA
*TkTheme: clam

! slightly modded TomorrowNightEighties
#define t_background #2d2d2d
#define t_foreground #eeeeee
#define t_current_line #eeeeee
#define t_selection #515151
#define t_comment #999999
#define t_red #f27777
#define t_orange #f99157
#define t_yellow #ffcc66
#define t_green #99cc99
#define t_aqua #66cccc
#define t_blue #6699cc
#define t_purple #cc99cc
*.foreground: t_foreground
*.background: t_background
*.cursorColor: t_green
! Black / Grey
*.color0: #000000
*.color8: #666666
! Red / Bright Red
*.color1: t_red
*.color9: #FF3334
! Green + Bright Green
*.color2: t_green
*.color10: #9ec400
! Yellow (Orange) + Bright Yellow (Yellow)
*.color3: t_orange
*.color11: t_yellow
! Blue + Bright Blue
*.color4: t_blue
*.color12: t_blue
! Magenta (Purple) + Bright Magenta
*.color5: t_purple
*.color13: #b777e0
! Cyan (Aqua) + Bright Cyan
*.color6: t_aqua
*.color14: #54ced6
! Light Grey (Selection) + White (Current Line)
!*.color7: t_selection
*.color7: t_comment
*.color15: t_current_line

! dmenu/rofi -----------------------------------------------------------------

#define dmenu_hl #99cc99
#define dmenu_fg #eeeeee
#define dmenu_bg #2d2d2d
dmenu.fn: DejaVu Sans Mono:size=10
dmenu.nf: dmenu_fg
dmenu.nb: dmenu_bg
dmenu.sf: dmenu_bg
dmenu.sb: dmenu_hl

rofi.color-enabled: true
! bg, fg, alt bg, highlight bg, highlight fg
rofi.color-normal: dmenu_bg,dmenu_fg,dmenu_bg,dmenu_hl,dmenu_bg
rofi.color-urgent: dmenu_bg,dmenu_hl,dmenu_bg,dmenu_hl,dmenu_bg
rofi.color-active: dmenu_bg,dmenu_hl,dmenu_bg,dmenu_hl,dmenu_bg
! bg, border, sep
rofi.color-window: dmenu_bg,dmenu_hl,dmenu_hl

rofi.opacity: 100
rofi.width: 35
rofi.lines: 8
rofi.columns: 1
rofi.loc: 0
rofi.xoffset: 0
rofi.yoffset: -2

rofi.separator-style: solid
rofi.bw: 2
rofi.padding: 2

rofi.font: DejaVu Sans Mono 10
rofi.fixed-num-lines: false
rofi.hmode: false
rofi.sidebar-mode: true
rofi.hide-scrollbar: true
rofi.parse-known-hosts: false
rofi.terminal: i3-sensible-terminal
rofi.threads: 0
rofi.combi-modi: window,drun

! xterm ----------------------------------------------------------------------

xterm*faceName: DejaVuSansMonoForPowerline Nerd Font:style=Book
xterm*faceSize: 10
xterm*allowBoldFonts: false
Xft.antialias: 1
Xft.autohint: 0
Xft.dpi: 96
Xft.hinting: 1
Xft.hintstyle: hintslight
Xft.lcdfilter: lcddefault
Xft.rgba: rgb

xterm*termName: xterm-256color
xterm*saveLines: 10000
xterm*borderWidth: 0
xterm*scrollTtyOutput: false
xterm*scrollKey: true
xterm*selectToClipboard: true
xterm*alternateScroll: true
xterm*VT100.translations: #override <Btn1Up>: select-end(PRIMARY, CLIPBOARD, CUT_BUFFER0)



! urxvt ----------------------------------------------------------------------

URxvt.font: xft:DejaVuSansMonoForPowerline Nerd Font:size=10
URxvt.boldFont:
URxvt.italicFont:
URxvt.scrollstyle: plain
URxvt.scrollColor: t_green
URxvt.scrollBar: false
URxvt.scrollBar_right: true
URxvt.saveLines: 8192
URxvt.scrollTtyOutput: false
URxvt.scrollWithBuffer: true
URxvt.scrollTtyKeypress: true
URxvt.cursorBlink: false
URxvt.secondaryScreen: 1
URxvt.secondaryScroll: 0
URxvt.secondaryWheel: 1
URxvt.print-pipe: "cat > /dev/null"

URxvt.perl-ext-common: selection-to-clipboard,confirm-paste,eval
URxvt.keysym.M-S-V: eval:paste_clipboard
URxvt.keysym.C-M-c: builtin-string:
URxvt.keysym.C-M-v: builtin-string:
URxvt.keysym.S-Page_Up: eval:scroll_up_pages 0.5
!URxvt.keysym.S-Page_Down: eval:scroll_down_pages 0.5
URxvt.keysym.S-Home: eval:scroll_to_top
URxvt.keysym.S-End: eval:scroll_to_bottom
!URxvt.keysym.S-Up: eval:scroll_up 1
!URxvt.keysym.S-Down: eval:scroll_down 1

! x11-ssh-askpass ------------------------------------------------------------

x11-ssh-askpass*background:             #99cc99
x11-ssh-askpass*foreground:             #2d2d2d
x11-ssh-askpass*Button.background:      #77aa77
x11-ssh-askpass*Indicator.foreground:   #2d2d2d
x11-ssh-askpass*Indicator.background:   #77aa77
x11-ssh-askpass*topShadowColor:         #99cc99
x11-ssh-askpass*bottomShadowColor:      #99cc99
x11-ssh-askpass*borderWidth:            0
x11-ssh-askpass*Dialog.title:           Enter Admin Credentials
x11-ssh-askpass*defaultXResolution:		40/in
x11-ssh-askpass*defaultYResolution:		40/in
