#!/usr/bin/env python3

from i3pystatus import Status
from i3pystatus.core.util import get_module
import stopwatch, oldnetwork, vpn

def status_configure():
    '''
    Configure statusbar
    '''
    status  = Status(standalone=True,logfile='$HOME/.tmp/.i3pystatus.log')

    span    = lambda x, c: "<span color=\""+c+"\">"+x+"</span>"
    xterm   = lambda c, t='': "alacritty -t '%s' -e %s"%(t,c)

    red     = "#f27777"
    orange  = "#f99157"
    green   = "#99cc99"
    nosep   = {"separator": False, "separator_block_width": 0}
    pango   = {"markup": "pango"}
    noseppango = {"separator": False, "separator_block_width": 0, "markup": "pango"}

    # Displays clock like this: 2015-10-16 22:35:69
    status.register("clock",
                    hints	        = noseppango,
                    on_rightclick   = xterm("sh -c 'calcardsync && khal interactive && calcardsync'", "calcardsync"),
                    interval        = 0.5,
                    format	        = ["  %H:%M:%S ", "  %Y-%m-%d %H:%M:%S ",],)

    # CPU temperature (Intel CPUs only)
    status.register("temp",
                    hints	        = noseppango,
                    on_rightclick   = "transmission-remote-gtk",
                    file	        = "/sys/class/thermal/thermal_zone2/temp",
                    alert_temp	    = 75,
                    format	        = span(" ({temp:.0f}°C) ", green),)
    # CPU usage
    status.register("cpu_usage",
                    hints	        = noseppango,
                    on_rightclick   = xterm("htop", "htop"),
                    format	        = span(" {usage:02}%", green),)
    status.register("cpu_usage_graph",
                    hints	        = noseppango,
                    on_rightclick   = xterm("htop", "htop"),
                    graph_width	    = 10,
                    start_color	    = green,
                    end_color	    = red,
                    format	        = span("{cpu_graph}", green),)

    # Network info
    @get_module
    def formatuptoggle(self):
        '''
        Switch network display
        '''
        formats = ["{tx_tot_Mbytes:>4} MiB  ¸{rx_tot_Mbytes:>5} MiB  ¸ {interface}    ",
                   "{bytes_sent:>4.0f} KB/s ¸{bytes_recv:>5.0f} KB/s ¸ {interface}    "]
        self.format_up = formats[(formats.index(self.format_up) + 1) % len(formats)]

    status.register(oldnetwork,
                    hints	        = noseppango,
                    on_leftclick    = formatuptoggle,
                    on_rightclick   = xterm("sudo -A nethogs wlan0", "nethogs"),
                    interface	    = "wlan0",
                    color_up        = green,
                    color_down      = red,
                    start_color	    = green,
                    end_color	    = red,
                    recv_limit	    = 1400,
                    sent_limit      = 90,
                    ignore_interfaces   = ['lo', 'sit0'],
                    format_up	    = "{bytes_sent:>4.0f} KB/s ¸{bytes_recv:>5.0f} KB/s ¸ {interface}    ",
                    format_down     = "down {interface}    ",)
    #  

    status.register(vpn,
                    hints	        = noseppango,
                    vpn_name        = lambda: open("/etc/openvpn/current", 'r').read().strip(),
                    on_leftclick    = "",
                    on_rightclick   = xterm("vpnselect", "vpnselect"),
                    on_upscroll     = "swaylock -i ~/wallpapers/Orbit.png",
                    status_up       = "",
                    status_down     = "",
                    interval        = 90,
                    color_up        = green,
                    color_down	    = red,
                    status_command  = "pgrep openvpn",
                    format          = "{status}",)

    """
    # Disk info
    disks, disk_fmt = ["/", "/home", "/mnt/homebak", ], "      {avail:0.1f}GiB (%s)"
    def diskcycle(self, d=1):
        disk_idx = (disks.index(self.path) + d) % len(disks)
        self.path = disks[disk_idx]
        self.format = disk_fmt % (disks[disk_idx])

    status.register("disk",
                    hints	        = noseppango,
                    path            = disks[0],
                    on_leftclick    = xterm("ranger"),
                    on_rightclick   = diskcycle,
                    on_upscroll     = [diskcycle, -1],
                    on_downscroll   = diskcycle,
                    critical_limit  = 8,
                    format          = disk_fmt % (disks[0]),)
    """

    # Stopwatch
    stopwatch_formats = [
        (3600, ' %h:%M:%S', green, ' %h:%M:%S '),
    ]
    status.register(stopwatch,
                    hints	        = noseppango,
                    interval        = 0.5,
                    color_paused    = green,
                    format_custom   = stopwatch_formats,
                    format_stopped  = "  00:00 ",
                    format_paused   = " %M:%S ",
                    format          = " %M:%S ",)
    status.register("text",
                    hints	        = nosep,
                    text            = "  ",)

    # Sound volume
    """
    status.register("alsa",
        hints	    = pango,
        format	    = " {volume} " + span("♪ ", green),)
    """
    # Media info
    playing_status = {
        "play": span(" ", green),
        "pause": span(" ", orange),
        "stop": span(" ", red),
    }

    @get_module
    def dbusvol(self, adj):
        '''
        Adjust dbus player volume
        '''
        vol = round(self.player_prop("Volume") * 100)
        self.player_prop("Volume", value=(vol + adj) / 100)

    playing_clicks = {
        "hints":            noseppango,
        "status":           playing_status,
        "on_leftclick":     ["player_command", "PlayPause"],
        "on_rightclick":    ["player_command", "Next"],
        "on_middleclick":   ["player_command", "Previous"],
        "on_upscroll":      ["player_command", "Seek", -10000000],
        "on_downscroll":    ["player_command", "Seek", +10000000],
    }
    status.register("now_playing",
                    **playing_clicks,
                    format	        = "\255  {status} smplayer",
                    player          = "smplayer",)
    status.register("now_playing",
                    **playing_clicks,
                    format	        = "\255  {status} kodi",
                    player          = "xbmc",)
    status.register("now_playing",
                    **playing_clicks,
                    format	        = "\255  {status} mpv",
                    player          = "mpv",)
    status.register("now_playing",
                    hints           = noseppango,
                    status          = playing_status,
                    on_leftclick    = ["player_command", "PlayPause"],
                    on_rightclick   = ["player_command", "Next"],
                    on_middleclick  = "quodlibet --show-window",
                    on_upscroll     = [dbusvol,  4],
                    on_downscroll   = [dbusvol, -4],
                    format	        = "\255  [{song_elapsed:%l%M:%S}/{song_length:%l%M:%S} - ]{volume}%{status} quod",
                    player          = "quodlibet",)

    # mpd info
    @get_module
    def mpdvol(self, adj):
        '''
        Adjust MPD volume
        '''
        vol = int(self._mpd_command(self.s, "status")["volume"])
        self._mpd_command(self.s, "setvol " + str(vol + adj))

    status.register("mpd",
                    hints           = noseppango,
                    on_rightclick   = "next_song",
                    on_middleclick  = xterm("ncmpcpp", "ncmpcpp"),
                    on_upscroll     = [mpdvol,  4],
                    on_downscroll   = [mpdvol, -4],
                    status 	        = playing_status,
                    hide_inactive   = True,
                    max_field_len   = 0,
                    max_len         = 100,
                    truncate_fields = ('album', 'artist'),
                    format	        = "[{song_elapsed:%l%M:%S}/{song_length:%l%M:%S} - ]{volume}%{status} mpd",)
    # "[{artist} / {album} / ]{title} / "

    status.run()

status_configure()
